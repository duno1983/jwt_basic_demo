package jwt1.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Lazy
@SpringBootApplication(scanBasePackages = { "jwt1.a" })
public class App {

	//为 Spring自带加密算法, 可用于JWT token加密/解密
	//供 SecurityConfig中注册 JwtFilter指定加密算法
    @Bean  
    public BCryptPasswordEncoder bCryptPasswordEncoder() {  
        return new BCryptPasswordEncoder();  
    }
    
    public static void main( String[] args ) {
		SpringApplication.run(App.class, args);
    }
}
