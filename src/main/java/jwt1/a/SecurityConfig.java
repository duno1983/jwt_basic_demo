package jwt1.a;

import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import jwt1.a.security.JwtEntryPoint;
import jwt1.a.security.JwtFilter;
import jwt1.a.security.TokenHelper;

/**
 * 说明：
 * 1 配置哪些请求路径需要 验证
 * 2 指定验证权限 filter： JwtFilter
 *
 */
@Configuration  
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)  
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;  
  
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
    private TokenHelper tokenHelper;
    
    @Autowired
    private JwtEntryPoint jwtEntryPoint;
  
    public SecurityConfig(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {  
        this.userDetailsService = userDetailsService;  
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;  
    }  
  
    @Override  
    protected void configure(HttpSecurity http) throws Exception {  

        List<RequestMatcher> csrfMethods = new ArrayList<>();
        Arrays.asList( "POST", "PUT", "PATCH", "DELETE" )
                .forEach( method -> csrfMethods.add( new AntPathRequestMatcher( "/**", method ) ) );
        http
                .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS )  //无状态 JWT
                .and().exceptionHandling().authenticationEntryPoint( jwtEntryPoint )            //异常处理
                .and().authorizeRequests()                                                      
	                .antMatchers(HttpMethod.POST, "/api/**").authenticated()                   //需要验证
	                .antMatchers(HttpMethod.GET, "/api/**").authenticated()
	                .antMatchers(HttpMethod.PATCH, "/api/**").authenticated()
	                .antMatchers(HttpMethod.DELETE, "/api/**").authenticated()
	            .and().authorizeRequests()
	                .antMatchers("/auth/**").permitAll()   //允许login/logout
	                .antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll() //允许 OPTIONS查询, for angularJS
                .and().addFilterBefore(new JwtFilter(tokenHelper, userDetailsService), BasicAuthenticationFilter.class)
                
                ;
                
        http.csrf().disable();  
    }  
  

    @Override
    public void configure(WebSecurity web) throws Exception {
        // TokenAuthenticationFilter will ignore the below paths

        web.ignoring()
        	.antMatchers(
	                "/auth/**",
                    "/",
        			"/demo/**", 
	        		"/resources/**",
	        		"/modules/**",
        			"/intg/**",//导入接口暂时不验证，因前端为链接形式 
        			"/test/**", //模拟客户数据接口        			
        			"/<**/**", //前端特殊请求: /%3Cdiv%20ui-view%3E%3C/div%3E"
                    "/**/*.html",
                    "/**/*.css",
                    "/**/*.eot",
                    "/**/*.ttf",
                    "/**/*.woff",
                    "/**/*.woff2",
                    "/**/*.js"
                );

    }
    
    @Override  
    public void configure(AuthenticationManagerBuilder auth) throws Exception {  
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);  
    }  
  
}
