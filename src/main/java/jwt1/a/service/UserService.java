package jwt1.a.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jwt1.a.domain.User;

@Service
public class UserService {
    
	Map<String, User> users = new HashMap<>();
	
	public UserService() {
		System.out.println("init UserService");
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		users.put("admin", new User("admin", passwordEncoder.encode("123")));
		users.put("test", new User("test", passwordEncoder.encode("123")));
		
	}
	
	
	public User findByName(String name) {
		return users.get(name);
	}
	
	public void save(User user) {
		if (user == null)
			return;
		users.put(user.getUsername(), user);
	}
}
