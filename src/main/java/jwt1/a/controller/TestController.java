package jwt1.a.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@CrossOrigin
    @RequestMapping(value="/api/list",  produces="application/json; charset=utf-8")
    public List<Integer> getAllFlight() throws Exception {
		return Arrays.asList(1, 2, 3, 4, 5);
	}}
