package jwt1.a.controller;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwt1.a.controller.dto.LoginDto;
import jwt1.a.domain.User;
import jwt1.a.security.TokenHelper;
import jwt1.a.service.UserService;

@RestController
public class JwtController {

    @Autowired
    private UserService userService;
    @Autowired
    private TokenHelper tokenHelper;

    @Autowired
    private AuthenticationManager authenticationManager;
    
	@CrossOrigin
    @RequestMapping(value = "/auth/login", method = {RequestMethod.GET, RequestMethod.POST})
    public String login(
            @RequestBody LoginDto req,
            HttpServletResponse response
    ) throws Exception {
		
		if (null == userService.findByName(req.getUsername())) {
			response.sendError(401, "username not exist");
		}
		
        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        req.getUsername(),
                        req.getPassword()
                )
        );

        // Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        // token creation
        User user = (User)authentication.getPrincipal();
        String jws = tokenHelper.generateToken( user.getUsername());
        int expiresIn = tokenHelper.getExpiredIn();
        
        ///TEST
        LocalDateTime expAt = LocalDateTime.now().plusSeconds(expiresIn);
        System.out.printf("/login %s, will expire on %s\n", user.getUsername(), expAt);
        
        // Return the token
        //return ResponseEntity.ok(new JwtTokenState(jws, expiresIn));
        return jws;
    }
}
