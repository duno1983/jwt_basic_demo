package jwt1.a.domain;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 说明：
 * 自定义部分
 * 		username
 * 		password
 * 继承自 UserDetails
 *		是否锁定
 *		是否过期
 *		是否生效
 *		是否密码过期
 */
public class User implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	String username;
	String password;
	
	public User() {}
	public User(String name, String password) {
		this.username = name;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;//细节权限，暂不实现
	}
	@Override
	public boolean isAccountNonExpired() {
		return true; //简单实现：不过期
	}
	@Override
	public boolean isAccountNonLocked() {
		return true; //简单实现：不锁
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;//简单实现：密码不过期
	}
	@Override
	public boolean isEnabled() {
		return true;//简单实现：总启用
	}
	
	
}
