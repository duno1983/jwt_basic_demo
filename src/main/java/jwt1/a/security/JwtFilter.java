package jwt1.a.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * 1. springboot http filter
 * 2. 入口  doFilterInternal()
 * 3. doFilterInternal()中 auth逻辑每一末端分支都打印log, 并 response.sendError()返回错误信息
 * 4. isRoleGrantedForUri(role, uri)用于区分某个role是否有权限访问目标操作uri 
 *
 */
public class JwtFilter extends OncePerRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(JwtFilter.class);
	
    private TokenHelper tokenHelper;

    private UserDetailsService userDetailsService;

    public JwtFilter(TokenHelper tokenHelper, UserDetailsService userDetailsService) {
        this.tokenHelper = tokenHelper;
        this.userDetailsService = userDetailsService;
    }


    @Override
    public void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
    	
    	//不检查 method=OPTIONS
    	if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
            chain.doFilter(request, response);
    		return;
    	}
    	//不检查 /auth/**
    	if (request.getRequestURI().startsWith("/auth")) {
            chain.doFilter(request, response);
    		return;
    	}
    	
    	//验证token
        String username;
        String authToken = tokenHelper.getToken(request);
        
        //后门token='12345678!@#$%^&*', for develop & debug
        if (authToken != null && authToken.equals("12345678!@#$%^&*")) {
            // create authentication
            UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
            JwtAuthentication authentication = new JwtAuthentication(userDetails);
            authentication.setToken(authToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
            return;
        }
        
        //正常验证流程
        if (authToken != null) {
            // get username from token
            username = tokenHelper.getUsernameFromToken(authToken);
            if (username != null) {
                // get user
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                if (tokenHelper.validateToken(authToken, userDetails)) {
                	//check role for uri
                    if (isRoleGrantedForUri(userDetails.getAuthorities(), request.getRequestURI())) {
                    	logger.info("{} user={} auth success", request.getRequestURI(), username);	
                        // create authentication
                    	JwtAuthentication authentication = new JwtAuthentication(userDetails);
                        authentication.setToken(authToken);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                    else {
                    	logger.info("{} need admin, user={} denied", request.getRequestURI(), username);
                    	response.sendError(403, String.format("access denied, %s only for admin", request.getRequestURI()));
                    	return;
                    }
                }
                else {
                	logger.info("{} token validate fail for user={}", request.getRequestURI(), username);
                	response.sendError(403, "token validate fail");
                	return;
                }
            }
            else {
            	logger.info("{} token->username is null", request.getRequestURI());
            	response.sendError(403, "invalid auth header value, can not retrive user from token");
            	return;
            }
        } else {
        	if (null == tokenHelper.getAuthHeaderFromHeader(request)) {
        		logger.info("{} header 'authorization' not found", request.getRequestURI());
            	response.sendError(403, "auth header not found");
            	return;
        	}
        	else {
        		logger.info("{} token is null, invalid header 'authorization' ", request.getRequestURI());
            	response.sendError(403, "invalid auth header value, can not parse token from header 'authorization'");
            	return;
        	}
        }
        chain.doFilter(request, response);
    }

	final static List<String> adminOnlyUris = Arrays.asList(
			"/api/scenario/publish",
			"/api/scenario/save",
			"/api/scenario/update",
			"/api/scenario/remove",
			"/api/user/save",
			"/api/shift/save",
			"/api/shift/delete/",
			"/api/workgroup/save",
			"/api/workgroup/delete",
			"/api/workgroup/allocate ",
			"/api/saveJSON",
			"/api/saveParameter/",
			"/api/saveStaff",
			"/api/staff/clear",
			"/api/saveDistance",
			"/api/aggregatedFlights/save",
			"/api/aggregatedFlights/generate",
			"/intg/");
	
    private boolean isRoleGrantedForUri(Collection<? extends GrantedAuthority> roles, String uri) {
    	boolean isAdminUri = false;
    	for (String item : adminOnlyUris) {
    		if (uri.startsWith(item)) {
    			isAdminUri = true;
    			break;
    		}
    	}
    	
    	if (!isAdminUri) {
    		return true;
    	}

		for (GrantedAuthority auth : roles) {
			if (auth.getAuthority().equalsIgnoreCase("admin")) {
				return true;
			}
		}
    	return false;
    }
}